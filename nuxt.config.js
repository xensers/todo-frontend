module.exports = {
  mode: "spa",
  render: {
    static: {
      setHeaders(res) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept"
        );
      },
    },
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "TODO Agency",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Branding & digital agency",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800&display=swap&subset=cyrillic",
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#90857F" },
  /*
   ** Global CSS
   */
  css: [
    "~/node_modules/normalize.css/normalize.css",
    "~/node_modules/swiper/dist/css/swiper.css",
    "~/assets/styles/main.scss",
  ],
  styleResources: {
    scss: ["~/assets/styles/_core.scss"],
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: "~/plugins/vue-awesome-swiper", ssr: false }],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    "@nuxtjs/style-resources",
    "@nuxtjs/axios",
    "@nuxtjs/proxy",
    [
      "@nuxtjs/yandex-metrika",
      {
        id: "45882408",
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true,
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
  proxy: [
    "http://wp.todoagency.ru/wp-json/",
    "http://wp.todoagency.ru/wp-content/",
  ],
};
