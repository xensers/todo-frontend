export const state = () => ({
  isOpen: false,
  color: null,
  list: [
    { title: 'Портфолио',  to: '/cases' },
    { title: 'Агентство', to: '/about' },
    { title: 'Контакты', to: '/contacts' }
  ]
});

export const mutations = {
  toggle(state) {
    state.isOpen = !state.isOpen;
  },
  open(state) {
    state.isOpen = true
  },
  close(state) {
    state.isOpen = false
  },
  toggleColor(state, newColor) {
    state.color = newColor
  }
};

export const actions = {
  toggleColor({ commit }, newColor) {
    commit('toggleColor', newColor);
  }
};
