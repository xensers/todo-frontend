import uniqueArrayValues from "../scripts/helpers/uniqueArrayValues";

let timerSaveList = null;

export const state = () => ({
  list: [],
  contents: {},
});

export const mutations = {
  setList(state, list) {
    state.list = list;
  },
  setContent(state, { id, content }) {
    state.contents[id] = content;
  },
  deleteItem(state, id) {
    const index = state.list.findIndex(el => el._id === id);
    state.list.splice(index, 1);
  },
  setItem(state, updatedItem) {
    const index = state.list.findIndex(el => el._id === updatedItem._id);
    state.list[index] = updatedItem;
  },
  itemTogglePublished (state, id) {
    const index = state.list.findIndex(el => el._id === id);
    state.list[index].published = !state.list[index].published;
  }
};

export const actions = {
  async fetchList({ commit }) {
    try {
      const list = await this.$axios.$get(`/wp-json/v1/cases`)
      commit('setList', list);
      return list;
    } catch (e) {
      throw e;
    }
  },

  async fetchContent({ commit }, id) {
    try {
      const content = await this.$axios.$get(`/wp-json/v1/cases/${id}`)
      commit('setContent', { id, content });
      return content;
    } catch (e) {
      throw e;
    }
  }
};

export const getters = {
  list: state => state.list.filter(item => item.published),
  itemById: state => id => state.list.find(item => item._id === id),
  itemByAlias: state => alias => state.list.find(item => item.alias === alias),
  contentById: state => id => state.contents[id],
  allTags: state => !!state.list.length ? uniqueArrayValues(state.list.map(item => item.tags || []).flat()) : [],
  allCategories: state => !!state.list.length ? uniqueArrayValues(state.list.map(item => item.categories || []).flat()) : [],
};
