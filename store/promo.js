export const state = () => ({
  list: [],
  title: [],
  titleMobile: [],
  subTitles: [],
  ending: "",
});

export const mutations = {
  setList(state, list) {
    state.list = list;
  },
  setTitle(state, title) {
    state.title = title;
  },
  setTitleMobile(state, titleMobile) {
    state.titleMobile = titleMobile;
  },
  setSubTitles(state, subTitles) {
    state.subTitles = subTitles;
  },
  setEnding(state, ending) {
    state.ending = ending;
  },
};

export const actions = {
  async fetchHome({ commit }) {
    try {
      const data = await this.$axios.$get(`/wp-json/v1/home`);
      commit("setList", data.categories);
      commit("setTitle", data.title);
      commit("setTitleMobile", data.title_mobile);
      commit("setSubTitles", data.subtitles);
      commit("setEnding", data.ending);
      return data;
    } catch (e) {
      throw e;
    }
  },
};

export const getters = {
  list: (state) => state.list,
  title: (state) => state.title,
  titleMobile: (state) => state.titleMobile,
  subTitles: (state) => state.subTitles,
  ending: (state) => state.ending,
};
