const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const formData = require("express-form-data")
const os = require("os");
const nodemailer = require('nodemailer');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(formData.parse({
  uploadDir: os.tmpdir(),
  autoClean: true
}));
app.use(bodyParser.raw({type: 'application/octet-stream'}))


const delay = timeout => new Promise(resolve => {
  setTimeout(() => {
    resolve()
  }, timeout || 0)
})

app.post('/test/:param', async (req, res, next) => {
  console.log(req.params)
  console.log(req.query)
  console.log(req.body)
  res.json({
    params: req.params,
    query: req.query,
    body: req.body
  })
})

app.post('/api/order', async (req, res) => {
  try {
    const transporter = nodemailer.createTransport({
      host: 'smtp.yandex.ru',
      port: 465,
      secure: true,
      auth: {
        user: 'xensers@yandex.ru',
        pass: 'bgrdff4y'
      }
    });


    let rows = '';
    for (let key in req.body) {
      rows += `<tr><td><b>${key}:</b></td><td>${req.body[key]}</td></tr>`
    }

    let html = `
      <h2 style="color: #241417;">
        Эти данные поступили с формы заявки на сайте todoagency.ru
      </h2>
      <br />
      <table>
        <tbody>
          ${rows}
        </tbody>
      </table>
    `;

    const data = {
      from: 'xensers@yandex.ru',
      to: "hello@todoagency.ru",
      subject: "Заявка с сайта todoagency.ru",
      html: html
    }

    let result = await transporter.sendMail(data);

    res.json({ status: 'successfully' });
  } catch (e) {
    console.error(e)
    res.status(500).json({ status: 'error' });
  }
})



module.exports = app
