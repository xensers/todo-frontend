export default function uniqueArrayValues(arr) {
  let result = [];
  arr.forEach(value => {
    if (!result.includes(value)) result.push(value);
  });
  return result;
}
