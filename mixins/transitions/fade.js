import anime from "animejs";

export default {
  methods: {
    onLeave(el, done) {
      const tl = anime.timeline({ easing: 'easeOutQuad' });
      tl.add({
        targets: el,
        opacity: [1, 0],
        duration: 500,
      });
      tl.finished.then(done)
    },
  },
  mounted() {
    anime({
      targets: this.$el,
      opacity: [0, 1],
      duration: 500,
      easing: 'easeOutQuad',
    });
  },
}
