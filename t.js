$(function () {
    $('.lazy').lazy();
    $('.header__menu-btn').on('click', function () {
        $(this).toggleClass('active');
        $('.header__menu').toggleClass('active');
    });
    $('.furniture__menu-btn').on('click', function () {
        $(this).toggleClass('active');
        $('.left-site').toggleClass('active');
    });
    $('.questions__item').on('click', function () {
        $(this).toggleClass('active');
    });
    $('.tabs .tab').on('click', function (event) {
        var id = $(this).attr('data-id');
        $('.tabs').find('.tab').removeClass('active');
        $(this).addClass('active');
        $('#' + id).addClass('active-tab').fadeIn();
        // return false;
    });
    $('select').styler();
    $(document).mouseup(function (e) {
        var element = $(".popup__wrapper, .popup-rev img");
        if (!element.is(e.target) && element.has(e.target).length === 0) {
            $(".popup, .popup-rev").removeClass("active");
            $(".popup-rev img").remove();
        }
        ;
    });
    $('.open-popup').on('click', function () {
        var curWeight = $(this).data('weight');
        var curVolume = $(this).data('volume');
        if (curWeight != null || curVolume != null) {
            $('.calcWeight').val(Number(curWeight)).trigger("keyup");
            $('.calcVolume').val(Number(curVolume)).trigger("keyup");
        }
        $('.popup').toggleClass('active');
    });
    $('.close').on('click', function () {
        $('.popup').removeClass('active');
    });
    $(".swiper-container-4 .swiper-slide").on("click", function () {
        var copy = $(this);
        copy.find('img').clone().appendTo('.popup-rev .container');
        $('.popup-rev').addClass('active');
    });
    $(".popup-rev img").on("click", function () {
        $('.popup-rev').removeClass('active');
    });

    //custom.js
    $('.header__menu > li').mouseover(function () {
        $(this).find('.sub_menu').stop(true, true).css('display', 'flex');
    }).mouseout(function () {
        $(this).find('.sub_menu').stop(true, true).hide();
    });
    $(".button1").on("click", function () {
        $('.buttons a').removeClass('button-active');
        $('.services__item').removeClass('active1 , active2')
        $(this).addClass('button-active');
    });
    $(".button2").on("click", function () {
        $('.buttons a').removeClass('button-active');
        $('.services__item').removeClass('active2')
        $('.services__item').addClass('active1')
        $(this).addClass('button-active');
    });
    $(".button3").on("click", function () {
        $('.buttons a').removeClass('button-active');
        $('.services__item').removeClass('active1')
        $('.services__item').addClass('active2')
        $(this).addClass('button-active');
    });
    $(window).resize(function () {
        var windowWidth = $('body').innerWidth();
        if (windowWidth < 725) {
            $('.services__item').removeClass('active1 , active2');
            $('.buttons').addClass('none');
        }
    });
    $('.open-popup.link').click(function () {
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var shirina = window.innerWidth;
        if (shirina < 1090) {
            $('.popup__wrapper').css("margin-top", scrollTop);
        }
        ;
    });
    $('#contact-middle').on('submit', function (e) {
        e.preventDefault();
        if (!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/contact.php",
            data: formentry,
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    alert('Сообщение успешно отправлено');
                } else {
                    console.log(data.errors);
                }
            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        })
        return false;
    });
    $('#contact-bottom').on('submit', function (e) {
        e.preventDefault();
        if (!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/contact.php",
            data: formentry,
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    alert('Сообщение успешно отправлено');
                } else {
                    console.log(data.errors);
                }
            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        })
        return false;
    });
    $('#calculate').on('submit', function (e) {
        e.preventDefault();
        if (!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/calculate.php",
            data: formentry,
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    alert('Сообщение успешно отправлено');
                } else {
                    console.log(data.errors);
                }
            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        })
        return false;
    });
    $('.toggleDirection').on('click', function () {
        var dirFrom = $(this).parent().find('.dirFrom').val();
        var dirTo = $(this).parent().find('.dirTo').val();
        $(this).parent().find('.dirFrom').val(dirTo);
        $(this).parent().find('.dirTo').val(dirFrom);
    });

    $('.calcInput').on('keyup', function () {
        this.value = this.value.replace(/[^0-9\.\,]/g, '').replace(/,/g, '.');
        var weight = Number($(this).parent().find('.calcWeight').val());
        var volumeOriginal = Number($(this).parent().find('.calcVolume').val());
        var volume = Number($(this).parent().find('.calcVolume').val() * 200);
        var delivery = Number($(this).parent().parent().parent().find('[name="deliveryType"]:checked').val());
        var dirFrom = $(this).parent().parent().parent().find('.dirFrom').val();
        var dirTo = $(this).parent().parent().parent().find('.dirTo').val();

        window.history.pushState("", "", "?weight=" + weight + "&volume=" + volumeOriginal + '&delivery=' + delivery + '&from=' + dirFrom + '&to=' + dirTo);

        // if (weight > volume) {
        //     var curAttr = weight;
        // } else {
        //     var curAttr = volume;
        //
        // }

        let curAttr = weight > volume ? weight : volume;
        let wl58 = curAttr < 58;

        switch (true) {
            case (curAttr >= 0 && curAttr <= 100):
                var exp = 600;
                if (wl58) {
                    curAttr = 400;
                } else {
                    curAttr *= 6.9;
                }
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 101 && curAttr <= 200):
                var exp = 700;
                curAttr *= 6.8;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 201 && curAttr <= 300):
                var exp = 800;
                curAttr *= 6.7;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 301 && curAttr <= 500):
                var exp = 900;
                curAttr *= 6.5;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 501 && curAttr <= 700):
                var exp = 1200;
                curAttr *= 6.4;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 701 && curAttr <= 1000):
                var exp = 1400;
                curAttr *= 6.2;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 1001 && curAttr <= 1500):
                var exp = 2000;
                curAttr *= 6;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 1501 && curAttr <= 2000):
                var exp = 2500;
                curAttr *= 5.8;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 2001 && curAttr <= 3000):
                var exp = 3500;
                curAttr *= 5.5;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 3001 && curAttr <= 5000):
                var exp = 6000;
                curAttr *= 4.9;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 5001):
                var exp = 6000;
                curAttr *= 4.9;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
        }
        if (curAttr < 400) {
            curAttr = 400;
        }
        $('.calcPrice').html(Math.round(curAttr * 100) / 100);
    });

    $('[name=deliveryType]').on('change', function () {
        var curPrice = Number($(this).parent().parent().parent().parent().parent().find('.calcPrice').text());
        var weight = Number($(this).parent().parent().parent().parent().parent().find('.calcWeight').val());
        var volumeOriginal = Number($(this).parent().parent().parent().parent().parent().find('.calcVolume').val());
        var volume = Number($(this).parent().parent().parent().parent().parent().find('.calcVolume').val() * 200);
        var delivery = this.value;
        var dirFrom = $(this).parent().parent().parent().parent().parent().find('.dirFrom').val();
        var dirTo = $(this).parent().parent().parent().parent().parent().find('.dirTo').val();

        window.history.pushState("", "", "?weight=" + weight + "&volume=" + volumeOriginal + '&delivery=' + delivery + '&from=' + dirFrom + '&to=' + dirTo);

        let curAttr = weight > volume ? weight : volume;
        let wl58 = curAttr < 58;

        switch (true) {
            case (curAttr >= 0 && curAttr <= 100):
                var exp = 600;
                if (wl58 == true) {
                    curAttr = 400;
                } else {
                    curAttr *= 6.9;
                }
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 101 && curAttr <= 200):
                var exp = 700;
                curAttr *= 6.8;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 201 && curAttr <= 300):
                var exp = 800;
                curAttr *= 6.7;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 301 && curAttr <= 500):
                var exp = 900;
                curAttr *= 6.5;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 501 && curAttr <= 700):
                var exp = 1200;
                curAttr *= 6.4;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 701 && curAttr <= 1000):
                var exp = 1400;
                curAttr *= 6.2;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 1001 && curAttr <= 1500):
                var exp = 2000;
                curAttr *= 6;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 1501 && curAttr <= 2000):
                var exp = 2500;
                curAttr *= 5.8;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 2001 && curAttr <= 3000):
                var exp = 3500;
                curAttr *= 5.5;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 3001 && curAttr <= 5000):
                var exp = 6000;
                curAttr *= 4.9;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
            case (curAttr >= 5001):
                var exp = 6000;
                curAttr *= 4.9;
                if (delivery == 2) {
                    curAttr = curAttr + exp;
                } else if (delivery == 3) {
                    curAttr = curAttr + (exp * 2);
                }
                break;
        }
        if (curAttr < 400) {
            curAttr = 400;
        }
        $('.calcPrice').html(Math.round(curAttr * 100) / 100);
    });
});
// script sliders //
var swiper = new Swiper('.swiper-container', {
    slidesPerView: 1,
    spaceBetween: 0,
    freeMode: true,
    loop: true,
    breakpoints: {
        1400: {
            slidesPerView: 3,
        },
        600: {
            slidesPerView: 2,
        },
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});
var swiper = new Swiper('.slider-container', {
    slidesPerView: 1,
    spaceBetween: 0,
    freeMode: true,
    loop: true,
    centeredSlides: true,
    centeredSlides: true,
    breakpoints: {
        1500: {
            slidesPerView: 6.3,
        },
        1400: {
            slidesPerView: 6,
        },
        1100: {
            slidesPerView: 5,
            spaceBetween: 20,
        },
        600: {
            slidesPerView: 3,
        },
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});
var swiper = new Swiper('.swiper-container-2', {
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        1500: {
            slidesPerView: 10,
        },
        1400: {
            slidesPerView: 9,
        },
        1100: {
            slidesPerView: 5,
            spaceBetween: 20,
        },
        600: {
            slidesPerView: 3,
        },
    },
});
var swiper = new Swiper('.swiper-container-3', {
    slidesPerView: 1,
    spaceBetween: 0,
    freeMode: true,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});
var swiper = new Swiper('.swiper-container-4', {
    slidesPerView: 1,
    spaceBetween: 40,
    freeMode: true,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        1500: {
            slidesPerView: 8,
        },
        1400: {
            slidesPerView: 6,
        },
        1100: {
            slidesPerView: 5,
            spaceBetween: 20,
        },
        600: {
            slidesPerView: 3,
        },
    },
});
var swiper = new Swiper('.swiper-container-5', {
    slidesPerView: 1,
    spaceBetween: 40,
    freeMode: true,
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        1250: {
            slidesPerView: 3,
        },
        800: {
            slidesPerView: 2,
        },
    },
});

// script sliders //
function validateForm(form) {
    var isValid = true;
    $(form).find('input').each(function (ind, el) {
        if (!$(el).data('required')) return;
        if ($(el).val() == '') {
            $(el).addClass('error');
            isValid = false;
        } else {
            $(el).removeClass('error');
        }
        // console.log($(el).attr('name'), isValid);
    });
    // console.log(isValid);
    return isValid;
}